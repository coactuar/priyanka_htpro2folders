<?php
require_once "logincheck.php";
$curr_room = 'games';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bg.jpg" usemap="#image-map">
            <map name="image-map">
               
            </map>
         <p style="position:absolute;left:20%;top:20%;color:white;font-size:20px;" onclick="gamescore(1)" >Game 1</p>
         <p style="position:absolute;left:50%;top:20%;color:white;font-size:20px;" onclick="gamescore(2)" >Game 2</p>
         <p style="position:absolute;left:20%;top:40%;color:white;font-size:20px;" onclick="gamescore(3)" >Game 3</p>
         <p style="position:absolute;left:50%;top:40%;color:white;font-size:20px;" onclick="gamescore(4)" >Game 4</p>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script>
function gamescore(a){
    alert(a);
    window.open('games/'+a+'', '_blank');
}


</script>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>