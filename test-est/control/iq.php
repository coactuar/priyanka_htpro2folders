<?php
require_once "../inc/config.php";
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {

        case 'getallques':

            $page_no = $_POST['pagenum'];
            $offset = ($page_no - 1) * $total_records_per_page;
            $previous_page = $page_no - 1;
            $next_page = $page_no + 1;

            $iq = new IQTest();
            $iq->__set('limit', $total_records_per_page);
            $total_records = $iq->getQuesCount();

            $total_no_of_pages = ceil($total_records / $total_records_per_page);
            $second_last = $total_no_of_pages - 1; // total page minus 1

            $quesList = $iq->getQuesList($offset);
            //var_dump($quesList);
            if (!empty($quesList)) {
?>
                <div id="message"></div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Question</th>
                            <th scope="col" width="470">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($quesList as $a) { ?>
                            <tr>
                                <td><?= $a['poll_question'] ?></td>
                                <td>
                                    <a class="btn btn-warning" href="iqques.php?ac=edit&id=<?= $a['poll_id'] ?>"><i class="far fa-edit"></i></a> <a class="btn btn-danger" href="#" onClick="delQues('<?= $a['poll_id'] ?>')"><i class="far fa-trash-alt"></i></a></td>




                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

                <ul class="pagination">
                    <?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } 
                    ?>

                    <li class='page-item <?php if ($page_no <= 1) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no > 1) {
                                                    echo "onClick='gotoPage($previous_page)' href='#'";
                                                } ?>>Previous</a>
                    </li>

                    <?php
                    if ($total_no_of_pages <= 10) {
                        for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                            if ($counter == $page_no) {
                                echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                            } else {
                                echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                            }
                        }
                    } elseif ($total_no_of_pages > 10) {

                        if ($page_no <= 4) {
                            for ($counter = 1; $counter < 8; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";
                            for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } else {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";

                            for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                        }
                    }
                    ?>

                    <li class='page-item <?php if ($page_no >= $total_no_of_pages) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no < $total_no_of_pages) {
                                                    echo "onClick='gotoPage($next_page)' href='#'";
                                                } ?>>Next</a>
                    </li>
                    <?php if ($page_no < $total_no_of_pages) {
                        echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>Last &rsaquo;&rsaquo;</a></li>";
                    } ?>
                </ul>
            <?php
            }

            break;

        case 'updatepoll':
            $poll_id = $_POST['pollId'];
            $session_id = $_POST['sessId'];
            $value = $_POST['value'];

            $value = (($value) ? 0 : 1);

            $poll = new Poll();
            $poll->__set('poll_id', $poll_id);
            $poll->__set('session_id', $session_id);
            $poll->__set('active', $value);
            $status = $poll->updateActivePoll();

            print_r(json_encode($status));

            break;

        case 'delques':
            $pollid = $_POST['quesId'];
            $poll = new IQTest();
            $poll->__set('poll_id', $pollid);
            $pollInfo = $poll->delPoll();

            print_r(json_encode($pollInfo));

            break;

        case 'getnextques':
            $user_id = $_POST['userId'];

            $poll = new IQTest();
            $poll->__set('user_id', $user_id);
            $currPoll = $poll->getNextQues();
            //var_dump($currPoll);
            if ($currPoll == '0') {
                echo '0';
            } else {
            ?>
                <form method="post">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <?= $currPoll[0]['poll_question'] ?>
                                </th>
                            </tr>
                        </thead>
                        <?php
                        $poll_opt[1] = $currPoll[0]['poll_opt1'];
                        $poll_opt[2] = $currPoll[0]['poll_opt2'];
                        $poll_opt[3] = $currPoll[0]['poll_opt3'];
                        $poll_opt[4] = $currPoll[0]['poll_opt4'];
                        $poll_opt[5] = $currPoll[0]['poll_opt5'];
                        ?>
                        <tbody>
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if (!empty($poll_opt[$i])) { ?>
                                    <tr>
                                        <td width="20"><input type="radio" id="iqopt<?= $i ?>" name="iqopts" class="form-check-input" value="opt<?= $i ?>"></td>
                                        <td align="left"><?= $poll_opt[$i] ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td>
                                    <div id="ques-res" style="display:none;"></div>
                                    <button type="button" id="respSub" name="iq_resp" data-user="<?= $user_id ?>" data-ques="<?= $currPoll[0]['poll_id'] ?>" class="send_iqresp btn btn-sm btn-primary">Submit</button>
                                    <small class="ml-3" style="display:none;">Next question is coming up in 5 secs...</small>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            <?php
            }

            break;

        case 'submitIQResp':
            $poll_id = $_POST['quesId'];
            $user_id = $_POST['userId'];
            $option = $_POST['answer'];

            $poll = new IQTest();
            $poll->__set('poll_id', $poll_id);
            $poll->__set('user_id', $user_id);
            $poll->__set('answer', $option);
            $pollResp = $poll->submitResponse();
            echo $pollResp;

            break;

        case 'getranks':
            $user_id = $_POST['userId'];

            $iq = new IQTest();
            $iq->__set('user_id', $user_id);
            $ranks = $iq->getRanks();
            //var_dump($ranks);
            $yourrank = 1;
            foreach ($ranks as $rank) {
                if ($user_id != $rank['user_id']) {
                    $yourrank++;
                } else {
                    break;
                }
            }

            //echo '<b>Your Rank: </b>' . $yourrank;

            $rankList = $iq->getRanksList();
            //var_dump($rankList);
            ?>
            <table class="table table-striped table-borderless">
                <thead>
                    <tr>
                        <td colspan="3"><b>Current Rank: </b> <?= $yourrank ?></td>
                    </tr>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Points</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($rankList as $rank) {
                        $user = new User();
                        $user->__set('user_id', $rank['user_id']);
                        $name = $user->getUserName();
                    ?>
                        <tr <?php if ($user_id == $rank['user_id']) {
                                echo 'style="background-color: rgb(221 53 130 / 50%); font-weight:500;"';
                            } ?>>
                            <td><?= $i++ ?></td>
                            <td><?= $name ?></td>
                            <td><?= $rank['total'] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
<?php

            break;
    }
}
