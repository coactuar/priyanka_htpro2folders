<?php
	require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions for Speaker</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body class="admin">
<div class="container-fluid main">
     <div class="row login-info links">   
        <div class="col-12 text-center">
            <div id="ques_update"></div>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <?php
            $sql = "SELECT COUNT(id) FROM tbl_questions where eventname='$event_name' and speaker='1' and answered='0'";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            ?>
            <input type="hidden" id="ques_count" name="ques_count" value="<?php echo $total_records; ?>">
            <div id="questions"> 
                <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="20"></th>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked At</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_questions where eventname='$event_name' and speaker='1' and answered='0' order by asked_at asc";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $i = 1;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_question']; ?></td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function getupdate()
{
	var curr = $('#ques_count').val();
	//alert(curr);
	
	$.ajax({ url: 'admin/ajax.php',
         data: {action: 'getspkquesupdate'},
         type: 'post',
         success: function(output) {
					 var msg = output;
                     
					 if (Number(output) > Number(curr))
					 {
						 newmsg = Number(output) - Number(curr);
						 if(newmsg == "1")
						 { 
						 msg = newmsg+ " new question available.";
						 }
						 else{
						 msg = newmsg+ " new questions available.";
						 }
						 msg += " <a href='speaker.php' class='btn btn-info btn-sm'> Refresh </a>";
					 }
					 else
					 {
						 msg="";
					 }
					 
					 	$("#ques_update").html(msg);
					 
                  }
});
}

setInterval(function(){ getupdate(); }, 10000);
</script>

</body>
</html>