<?php
	include 'config.php';
?>

<!doctype html>
<html lang="en">
  <head>
    <title>AutoMailer</title>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
      *{
          font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
      }
    #btnSubmit{
          background-color:orange;
      }
  </style>

</head>
  <body>
      <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <form  id="reg-form" method="post" class="form-group text-center">
                           <img src="COACT LOGO_1 Powered By.gif" class="coact mt-5" alt="Powered By Coact" style="height:77px;margin-top:12px;"></a>   
                           <h3 class="mt-5">Register for GetMail</h3>
                           <input type="hidden" id="app" name="app" value="<?= $varified; ?>"> 
                            <div class="row mt-4">   
                                <div class="col-md-6">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                                </div>
                                <div class="col-md-6">
                                    <label for="number">Mobile No.</label>
                                    <input type="number" class="form-control" name="number" id="number" placeholder="Mobile No.">
                                </div>                            
                            </div>  
                            <div class="row">
                                <div class="col-md-6">   
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                </div>
                                <div class="col-md-6">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">   
                                    <label for="dob">Date of Birth</label>
                                    <input type="date" class="form-control" name="dob" id="dob" placeholder="Date Of Birth">
                                </div>
                                <div class="col-md-6">   
                                    <label for="dob">Address</label>
                                    <textarea type="text" class="form-control" name="address" id="address" placeholder="Address"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="gender">Gender</label><br>
                                    
                                    <label for="other">Other</label>
                                    <input type="radio" name="gender" value="Other">
                                  
                                    <label for="female">Female</label>
                                    <input type="radio" name="gender" value="Female">
                                   
                                    <label for="male">Male</label>
                                <input type="radio" name="gender" value="Male">
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Select Known</label>
                                    <select class="selectk form-control" id="selectk">
                                        <option>option 1</option>
                                        <option>option 2</option>
                                        <option>option 3</option>
                                        <option>option 4</option>
                                        <option>option 5</option>
                                    </select>
                                </div>
                               
                            </div><br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit" style="width:100px;height:50px" value="Submit" />
                                </div>
                            </div>
                        </form>
                    </div>
                <div class="col-md-3"></div>
            </div>
      </div>
      <div id="code">IPL/O/BR/09042021</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4> <div id="login-message"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

   
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script>
  $(document).on('submit', '#reg-form', function()
{  
  $.post('save.php', $(this).serialize(), function(data)
  {
      console.log(data);
      if(data == 's')
      {
        $('#login-message').text('You are registered succesfully for the coact Live. Please check your email regarding event Details.');
        $('#login-message').addClass('alert-success');
		$("#exampleModalCenter").modal('show');
		 $('#btnSubmit').fadeOut(); 
		   $('#btnSubmit').delay(5000).fadeIn();
        
          return false;
      }
      else if (data == '1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		   $('#btnSubmit').fadeOut(); 
		     $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		  $("#exampleModalCenter").modal('show');  
		  $('#btnSubmit').fadeOut(); 	  
		  $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});
</script>


</body>
</html>