<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = 'eb30708198e67de26b3121aa65d4db494f0948fb20d89dcf8b0a994c841fc11f';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'room1';
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/img/room.jpg" usemap="#image-map">
            <map name="image-map">
                <area onclick="showAttendees()" alt="Attendee List" title="Attendee List" href="#" coords="323,247,922,357,927,530,331,550" shape="poly">
                <area target="_blank" alt="Join Meeting" title="Join Meeting" href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODM1M2Q0YjgtNzgxZC00Yzk1LWE2ODktMDk5MGQ0MzQ0ZWRl%40thread.v2/0?context=%7b%22Tid%22%3a%225d2d4761-845f-4e1c-a2cd-aaded29aaf65%22%2c%22Oid%22%3a%22081250f1-384a-463f-bf60-6b9c196be8a4%22%7d" coords="1161,394,1156,481,1478,471,1480,369" shape="poly">
                <area target="_blank" alt="Miro Whiteboard" title="Miro Whiteboard" href="https://miro.com/app/board/o9J_kgxIQHg=/" coords="1498,368,1498,473,1702,470,1702,351" shape="poly">
            </map>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>

<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);

    function showAttendees() {
        $.magnificPopup.open({
            items: {
                src: 'assets/resources/list1.jpg'
            },
            type: 'image'
        });
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>