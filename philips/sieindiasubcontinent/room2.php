<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = '13f2f4bd79d8d7f97aafa8bd7604d56b72986f486ce9fc1d6a49a1b63daa18c4';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'room2';
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <!-- <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div> -->
        <div id="bg">
            <img src="assets/img/room.jpg" usemap="#image-map">
            <map name="image-map">
                <area onclick="showAttendees()" alt="Attendee List" title="Attendee List" href="#" coords="323,247,922,357,927,530,331,550" shape="poly">
                <area target="_blank" alt="Join Meeting" title="Join Meeting" href="https://teams.microsoft.com/dl/launcher/launcher.html?url=%2F_%23%2Fl%2Fmeetup-join%2F19%3Ameeting_NWYwMDkwMDAtYmI5Mi00OWZjLTg4MWMtYWUwODMwMjkwZWNh%40thread.v2%2F0%3Fcontext%3D%257b%2522Tid%2522%253a%25221a407a2d-7675-4d17-8692-b3ac285306e4%2522%252c%2522Oid%2522%253a%252276dc9e8a-05e7-4a01-afd9-23c9c44938c4%2522%257d%26anon%3Dtrue&type=meetup-join&deeplinkId=a99ba66d-5c5d-4885-9c7a-ac00067be5d3&directDl=true&msLaunch=true&enableMobilePage=true&suppressPrompt=true" coords="1161,394,1156,481,1478,471,1480,369" shape="poly">
                <area target="_blank" alt="Miro Whiteboard" title="Miro Whiteboard" href="https://miro.com/app/board/o9J_kgxZqpY=/" coords="1498,368,1498,473,1702,470,1702,351" shape="poly">
            </map>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>

<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);

    function showAttendees() {
        $.magnificPopup.open({
            items: {
                src: 'assets/resources/list2.jpg'
            },
            type: 'image'
        });
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>