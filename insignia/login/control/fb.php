<?php
require_once "../inc/config.php";
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {
        case 'getallfeedbacks':
            $page_no = $_POST['pagenum'];
            $offset = ($page_no - 1) * $total_records_per_page;
            $previous_page = $page_no - 1;
            $next_page = $page_no + 1;

            $fb = new Feedback();
            $fb->__set('limit', $total_records_per_page);
            $total_records = $fb->getFbCount();

            $total_no_of_pages = ceil($total_records / $total_records_per_page);
            $second_last = $total_no_of_pages - 1; // total page minus 1
?>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <td scope="col">Total Feedbacks: <div id="ques_count" style="display:inline-block;"><?php echo $total_records; ?></div>
 <td scope="col"><a href="fb.php">Download Feedbacks</a></td>

                            </th>
                                            </tr>
                </thead>
            </table>
            <?php
            $fbList = $fb->getAllFeedbacks($offset);
            //var_dump($fbList);

            if (!empty($fbList)) {
            ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" width="1000">Name</th>
                            <th scope="col">Email Id</th>
                            <th>Please rate the overall aspects of this educational activity based on:
</th>
                            <th scope="col">Dr Betul Hatipoglu </th>
                            <th scope="col">Dr Rita Basu</th>
                            <th scope="col">Dr Dennis Bruemmer </th>
                            <th scope="col">Dr Spyridoula Maraka </th>
                            <th scope="col">Dr Anand Vaidya </th>
                            <th scope="col">Dr Natalie Cusano </th>
                            <th scope="col">Dr Richard Auchus</th>
                            <th scope="col">Dr Maria Fleseriu </th>
                            <th scope="col">Dr Betul Hatipoglu </th>
                            <th scope="col">Dr Rita Basu</th>
                            <th scope="col">Dr Dennis Bruemmer </th>
                            <th scope="col">Dr Spyridoula Maraka </th>
                            <th scope="col">Evaluation and Management of Suspicious Adrenal Masses</th>
                            <th scope="col">Mild Forms of Hyperparathyroidism [Including Normocalcemic]</th>
                            <th scope="col">Approach to Patient with Adrenal Insufficiency</th>
                            <th scope="col">Personalized Treatment in Acromegaly</th>
    
                            <!-- <th scope="col">Q 26</th> -->
                            <th scope="col">What influenced you to attend this meeting?</th>
                            <th scope="col">Do you have any other feedback?</th>
                           
                            <th scope="col">Feedback Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($fbList as $a) { ?>
                            <tr>
                                <td><?=  $a['first_name'] . ' ' . $a['last_name'] ?></td>
                                <td><?= $a['emailid'] ?></td>
                                <td><?= $a['q26'] ?></td>
                                <td><?= $a['q05'] ?></td>
                                <td><?= $a['q06'] ?></td>
                                <td><?= $a['q07'] ?></td>
                                <td><?= $a['q08'] ?></td>
                                <td><?= $a['q09'] ?></td>
                                <td><?= $a['q10'] ?></td>
                                <td><?= $a['q11'] ?></td>
                                <td><?= $a['q12'] ?></td>
                                <td><?= $a['q01'] ?></td>
                                <td><?= $a['q02'] ?></td>
                                <td><?= $a['q03'] ?></td>
                                <td><?= $a['q04'] ?></td>
                                <td><?= $a['q13'] ?></td>
                                <td><?= $a['q14'] ?></td>
                                <td><?= $a['q15'] ?></td>
                             
                                <td><?= $a['q16'] ?></td>
                                <td><?= $a['q27'] ?></td>
                                <td><?= $a['q28'] ?></td>
                             
                                <td><?php
                                    $date = date_create($a['feedback_time']);
                                    echo date_format($date, "M d, H:i a");
                                    ?>
                                </td>







                            </tr>
                        <?php } ?>
                    </tbody>

                </table>
                <ul class="pagination">
                    <?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } 
                    ?>

                    <li class='page-item <?php if ($page_no <= 1) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no > 1) {
                                                    echo "onClick='gotoPage($previous_page)' href='#'";
                                                } ?>>Previous</a>
                    </li>

                    <?php
                    if ($total_no_of_pages <= 10) {
                        for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                            if ($counter == $page_no) {
                                echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                            } else {
                                echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                            }
                        }
                    } elseif ($total_no_of_pages > 10) {

                        if ($page_no <= 4) {
                            for ($counter = 1; $counter < 8; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";
                            for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } else {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";

                            for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                        }
                    }
                    ?>

                    <li class='page-item <?php if ($page_no >= $total_no_of_pages) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no < $total_no_of_pages) {
                                                    echo "onClick='gotoPage($next_page)' href='#'";
                                                } ?>>Next</a>
                    </li>
                    <?php if ($page_no < $total_no_of_pages) {
                        echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>Last &rsaquo;&rsaquo;</a></li>";
                    } ?>
                </ul>
<?php
            } else {
                echo 'No feedbacks given yet.';
            }
            break;
    }
}
