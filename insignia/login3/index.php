<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';
$fullname = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  if ($_POST['emailid'] === '') {
    $errors['email'] = 'Email ID is required';
  }
  if ($_POST['fullname'] === '') {
    $errors['fullname'] = 'Full name is required';
  }

  $emailid = $_POST['emailid'];
  $fullname = $_POST['fullname'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $user->__set('fullname', $fullname);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $event_title ?></title>
  <link rel="stylesheet" href="assets/css/normalize.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/all.min.css">
  <link rel="stylesheet" href="assets/css/styles.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
.modal-body h1 {
  font-weight: 900;
  font-size: 2.3em;
  text-transform: uppercase;
}
.modal-body a.pre-order-btn {
  color: #000;
  background-color: gold;
  border-radius: 1em;
  padding: 1em;
  display: block;
  margin: 2em auto;
  width: 50%;
  font-size: 1.25em;
  font-weight: 6600;
}
.modal-body a.pre-order-btn:hover {
  background-color: #000;
  text-decoration: none;
  color: gold;
}


</style>
<body id="login">
  <div class="container reg-content">
    <div class="row p-4">
      <!-- <div class="col-12 col-lg-12 mx-auto text-center">
        <div class="row pt-4"> -->
          <div class="col-12 col-md-12  text-center">
            <img src="assets/img/Best_text.png" class="" width="300px" alt="">
          </div>
        </div>
        <div class="row ">
          <div class="col-12 col-md-12  text-center ">
            <img src="assets/img/Best_text3.png" class="" width="300px" alt="">
          </div>
        </div>
          <!-- <div class="col-4 col-md-3  text-center p-3">
            <img src="assets/img/logo-aace.png" class="img-fluid" alt="">
          </div> -->
        </div>
      </div>
    </div>
    <!-- <div class="row mt-3">
      <div class="col-6 col-md-6 col-lg-4 mx-auto">
        <img src="assets/img/block-your-date.png" class="img-fluid" alt="">
      </div>
    </div> -->
    <div class="register-wrapper mx-4">
      <div class="row">
        <div class="col-12 col-md-8 mx-auto p-2">
          <!-- <h5 class="text-center my-2">Log in to the first virtual symosium of CARBS Summit</h5> -->
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 mx-auto p-2">
          <?php
          if (count($errors) > 0) : ?>
            <div class="alert alert-danger alert-msg">
              <ul class="list-unstyled">
                <?php foreach ($errors as $error) : ?>
                  <li>
                    <?php echo $error; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>
          <?php if ($succ != '') { ?>
            <div class="alert alert-success alert-msg">
              <?= $succ ?>
            </div>
          <?php } ?>
          <form method="POST" class="mt-4">

            <div class="row mt-3 mb-1">
            <div class="col-12">
                <label> Full name</label>
                <input type="text" name="fullname" id="fullname" class="input" placeholder="TO BE USED ON CERTIFICATE" value="<?= $emailid ?>">
              </div>
              <div class="col-12">
                <label>Email ID</label>
                <input type="text" name="emailid" id="emailid" class="input" placeholder="ENTER YOUR REGISTERED EMAIL ID" value="<?= $emailid ?>">
              </div>
            </div>
            <div class="row mt-4 mb-3">
              <div class="col-12">
                <input type="image" src="assets/img/btn-login.png" value="Submit" />
                <br><br>
                If not registered, <a href="https://bestofaace2021.com/">click here</a>
              </div>
            </div>
        </div>
        </form>
      </div>
      <div class="row">
        <div class="col-12 col-md-4 offset-md-3 small_logo">
          <img src="assets/img/small_logo.png" class="" width="200px" alt="" />
        </div>
        <div class="col-12 col-md-4 small_logo">
          <img src="assets/img/small_logo1.png" class="" width="200px" alt="" />
        </div>
      </div>
    </div>
  </div>
    <!-- <div class="modal fade" id="modal-subscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header border-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <h1 class="text-center text-uppercase">Enter your full name for Certifiaction </h1>
            <div class="row">
              <div class="col-md-12  col-12 ">
              <form action="">
  <div class="col-md-8 offset-md-2">
  <input type="text" class="form-control mb-2" placeholder="Enter your full name" required>
  <button type="submit" class="btn btn-primary mb-2 text-white btn-md">
  Enter
  </button> -->
</div>
            
            </form>
							<!-- <div class="form-group center-block">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Enter your full name" required>
              
                  <input type="submit" class="btn btn-primary ml-2 text-white btn-md" value="Enter">
                
									<span class="input-group-btn"><a class="btn btn-primary ml-2 text-white btn-md">Enter</a></span>
								</div>
							</div> -->
            </div>
              <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 
<script>
// $ (window).ready (function () {
// 	setTimeout (function () {
// 		$ ('#modal-subscribe').modal ("show")
// 	}, 3000)
// })
</script>
  </div>
  <?php require_once 'ga.php';  ?>
  <?php require_once 'footer.php';  ?>

  