"use strict";

var script = $('script[src*=site]'); // or better regexp to get the file name..

var loc = script.attr('data-loc');   
if (typeof loc === "undefined" ) {
   loc = '';
}


$(window).on('load', function () {
    $('#preloader').fadeOut('slow', function () {
        $(this).remove();
    });
    updateEvent(loc);
    getLiveAttendees(loc);
    getPageAttendees(loc);
    setInterval(function(){ updateEvent(loc); }, 5000);
    setInterval(function(){ getLiveAttendees(loc); }, 3000);
    setInterval(function(){ getPageAttendees(loc); }, 4000);

});

$('.play-icon i').click(function () {
    var video = '<iframe allowfullscreen src="' + $(this).attr('data-video') + '"></iframe>';
    $(this).replaceWith(video);
});

function updateEvent(loc)
{
    $.ajax({ url: 'controls/server.php',
         data: {action: 'update-event', room: loc},
         type: 'post',
         success: function(output) {
			   if(output==="0")
			   {
				   location.href='index.php';
			   }
         }
});
}

function getLiveAttendees(loc)
{
    $.ajax({ url: 'controls/server.php',
         data: {action: 'getliveatt', room: loc},
         type: 'post',
         success: function(output) {
			   $('#total-att').html(output);
         }
});
}

function getPageAttendees(loc)
{
    $.ajax({ url: 'controls/server.php',
         data: {action: 'getonpageatt', room: loc},
         type: 'post',
         success: function(output) {
			   $('#page-att').html(output);
         }
});
}


