<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Mammography</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
::-webkit-input-placeholder { /* Edge */
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
 
  
}

:-ms-input-placeholder { /* Internet Explorer */
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
  
}

::placeholder {
  color: black !important;
  font-weight: 400;
  font-family: Verdana, sans-serif;
  
}


</style>
</head>

<body>
<div class="container-fluid mr-1">

    <div class="row ">
      <div class="col-12 col-md-8  p-md-0 col-lg-8">
         <img src="img/Mammography.jpg" width="100%" height="100%vh"  alt=""/>
      </div>
      <div class="col-12 col-md-6 col-lg-4 text-center mt-md-2">
		<div class="container">
        <div class="login text-center">
        <img src="img/Registration_header_Page.png" class="img-fluid mb-1" alt=""/>
				<form id="login-form" method="post" role="form">
						<div id="login-message"></div>
				  <div class="form-group text-left">
					
          <!-- <input type="checkbox" value="1" name="miss" /> Miss &nbsp
    <input type="checkbox" value="1" name="ms" /> Mrs &nbsp
 
    <input type="checkbox"  value="1" name="mr" /> Mr &nbsp
    <input type="checkbox"  value="1" name="dr" /> Mr &nbsp -->
					<input type="checkbox" value="1"   name="miss"> Miss &nbsp
                    <input type="checkbox"  value="1"   name="ms"> Mrs &nbsp
                    <input type="checkbox"   value="1"   name="mr"> Mr &nbsp
                    <input type="checkbox"  value="1"   name="dr"> Dr 
				
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="First Name:" name="fname" id="fname" required>
                  </div>
				  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Last Name:" name="lname" id="lname" required>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Organisation: " name="organisation" id="company" required>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="City:" name="city" id="city" required>
                  </div>
				  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Mobile:" name="mobnum" id="designation" required>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email Address:" name="email" id="email" required>
                  </div>
				  <div class="form-group">
                  <p  style="font-size:10px; text-align:left">By registering you acknowledge your consent to continue and agree not to share or  distribute information from this domain</p>
				  </div>
				  <div class="form-group">
				  <p  style="font-size:10px; text-align:left"><input type='checkbox' name='concent' required> I hereby consent to the processing of my above given personal data by the Siemens Healthineers company as referred to under Corporate Information for the purposes as described in the <u><a href="https://events.siemens-healthineers.com/declaration-of-consent-to-enter-the-virtual-event-platform">Declaration of Consent to enter the virtual event platform</a></u>. I have also read the further information regarding the processing of my personal data contained in the <u><a href="https://events.siemens-healthineers.com/event-platform-privacy-policy">Data Privacy Policy.</a></u>
					<br/>I am aware that I can partially or completely revoke this consent at any time for the future by declaring my revocation to the contact address given in the Corporate information or by sending it to the following e-mail address: <u>dataprivacy.communication@siemens-healthineers.com</u></p>
				  </div>
				   <div class="form-group">
				  <p  style="font-size:10px; text-align:left"><input type='checkbox' name='aggriment' required> I hereby confirm that I agree with and adhere to the <u>special Terms of Use for the Siemens Healthineers Event Website.</u>
					</p>
				  </div>
				  <!--
				  <div class="form-group">
                  <p  style="font-size:9px; text-align:left;font-style: italic;">The protection of your data and recognition of your rights with regards to the collection, processing, and use of your data are important to Siemens Healthineers. As part of this event, Siemens Healthineers may process your personal data and pass it on to third parties. This is limited to personal data that you actively and voluntarily provide during the registration process.</p>
				  <p  style="font-size:9px; text-align:left;font-style: italic; ">The collected data will only be passed on to the third parties to provide services and functions in relation to this event, to verify your identity, and to answer and fulfill your specific requests e.g. digital virtual, hybrid or live event participation and similar services. Any data collected in relation to this event shall be handled in accordance with Siemens Healthineers Data Privacy Notice.</p>
				  </div>
				  <div class="form-group">
				  <p  style="font-size:9px; text-align:left"><input type='checkbox' name='concent' required> I hereby consent to the processing of my above given personal data by the Siemens Healthineers company as Referred to under corporate information for the purposes as described in the <u><a href="https://events.siemens-healthineers.com/declaration-of-consent-to-enter-the-virtual-event-platform">Declaration of consent to enter the Virtual Event platform.</a></u>. I have also read the further information regarding the processing of my personal data contained in the <u><a href="https://events.siemens-healthineers.com/event-platform-privacy-policy">Data Privacy Policy.</a></u>
					<br/>I am aware that I can partially or completely revoke this consent at any time for the future by declaring my revocation to the contact address given in the Corporate information or by sending it to the following e-mail address: <u>dataprivacy.communication@siemens-healthineers.com</u></p>
				  </div>
				   -->
                  <div class="form-group mt-4 text-left">
                  <input type="image" id="login" src="img/btn-register -new.png" width="150px" value="submit"> 
                  </div>
				  <div class="form-group mt-4 text-left">
				   <a href="products.php"><img class="img-fluid" src="img/below submit.png" style="width:170px" ></a>
				  </div>	
				  <div class="form-group mt-4 text-left">
				   <a href="login.php" class="btn btn-info">Login</a>
				  </div>	
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  $('#login').attr('disabled', true);
  $.post('register-data.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          $('#login-message').text("You are registered successfully.");
          $('#login-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
          $('#login-form').trigger("reset");
          $('#login').attr('disabled', false);
          return false;  
      }
      
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
</script>
<script>

$("input:checkbox").on('click', function() {

var $box = $(this);
if ($box.is(":checked")) {

  var group = "input:checkbox[value='" + $box.attr("value") + "']";

  $(group).prop("checked", false);
  $box.prop("checked", true);
} else {
  $box.prop("checked", false);
}
});
</script>
</body>
</html>