<?php

namespace App\Http\Livewire;
use Carbon\Carbon;
use Livewire\Component;
use App\Models\Commnet;
use Livewire\WithPagination;
use Livewire\WithFileUploads;


class Comments extends Component
{
    use WithPagination;
    use WithFileUploads;
    //protected $paginationTheme = 'bootstrap';
   // public $comments;
    public $newComment;
    public $image;

    public function mount(){
       // $this->newComment = 'this is from mounted comment';
      // dd($initialComments);
    // $initialComments = Commnet::all();
    //  $initialComments = Commnet::latest()->get();
  
    // $this->comments = $initialComments;
    
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, ['newComment' => 'required|max:255']);
    }




    public function addComment(){
        //proper validation starts 
        $this->validate(['newComment' => 'required|max:255']);
        //proper validation ends 
        // if($this->newComment == ''){
        //     return;
        // }

        $CreatedComment = Commnet::create(['body' => $this->newComment,
        'user_id' => 1
        ]);
        //$this->comments->push($CreatedComment);
      //  $this->comments->prepend($CreatedComment);
        $this->newComment = "";
        session()->flash('message','Commend addded successfully 😄');
    }

    public function remove($commentId){
        $comment = Commnet::find($commentId);
        $comment->delete();
        //$this->comments = $this->comments->where('id','!=', $commentId); 
        
        //$this->comments = $this->comments->except($commentId); 
            //dd($commentId);
        session()->flash('message','Commend removed successfully 😅');
    }


    public function render()
    {
      
        return view('livewire.comments',[
           'comments' => Commnet::latest()->paginate(1)
        ]);
    }
}
