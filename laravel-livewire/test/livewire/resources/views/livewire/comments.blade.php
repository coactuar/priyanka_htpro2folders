<div>
@error('newComment') <span class="text-red-500 text-xs">{{ $message }}</span> @enderror

<input type="file" name="image" id="image" wire:change="$emit('fileChoosen')" >

 <form wire:submit.prevent="addComment">
 <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>

<input type="text" name="comments_input" wire:model.debounce.500ms="newComment">
   <button type="submit">add</button>
   </form>
   @foreach($comments as $comment)
   <div class="close"><a wire:click="remove({{$comment->id}})">X</a></div>
   <div class="comment_class">{{$comment->body}}</div>
   <div class="timecreated">{{$comment->created_at->diffForHumans()}}</div>
   <div class="author">{{$comment->creator->name}}</div>
    @endforeach
    {{ $comments->links('pagination-links') }}
</div>
<script>
Livewire.on('fileChoosen', () => {
   // alert('Image uploaded ');
    let inputFields = document.getElementById('image')
    //console.log(file.files[0]);
    let file = inputFields.files[0]
    let reader = new FileReader();
    reader.OnLoadend = () => {
        console.log(reader.result);
    }
    reader.readAsDataURL(file);
})
</script>