<?php
require_once '../functions.php';
require_once 'logincheck.php';
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>

<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12">
            <a href="res.php?ac=add" class="btn btn-sm btn-primary">Add Resource</a>
        </div>
    </div>
    <div class="row p-2">
        <div class="col-12">

            <div id="resources">
                <?php
                $exhib = new Exhibitor();
                $resList = $exhib->getResources();
                //var_dump($resList);
                if (!empty($resList)) {
                ?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Resource Title</th>
                                <th scope="col" width="200">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($resList as $a) { ?>
                                <tr>
                                    <td><?= $a['resource_title']; ?>
                                        <!-- <br><?= $a['resource_id']; ?><br><?= $a['resource_url']; ?> -->
                                    </td>
                                    <td><a href="res.php?ac=edit&id=<?= $a['resource_id']; ?>" class="btn btn-warning">Edit</a> <a onclick="delres('<?= $a['resource_id']; ?>')" class="btn btn-danger">Delete</a> </td>
                                </tr>
                            <?php } ?>
                        </tbody>

                    </table>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

</div>

<?php
require_once 'scripts.php';
?>
<script>
    function delres(res_id) {
        if (confirm('Are you sure?')) {
            $.ajax({
                url: '../control/exhib.php',
                data: {
                    action: 'delres',
                    res_id: res_id
                },
                type: 'post',
                success: function(message) {
                    var response = JSON.parse(message);
                    var status = response['status'];
                    var msg = response['message'];
                    if (status == 'success') {
                        location.href = 'resources.php';
                    } else {
                        $('#message').html(msg).removeClass().addClass('alert alert-danger').fadeIn();
                    }
                }
            });
        }
    }
</script>
<?php
require_once 'footer.php';
?>