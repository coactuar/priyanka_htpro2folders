<?php
require_once "logincheck.php";
$curr_room = 'bcsjourney';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<link href='assets/css/simplelightbox.min.css' rel='stylesheet' type='text/css'>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bcsjourney.jpg">
            <div id="bcsGallery">
                <div class="gallery">
                    <?php
                    // Image extensions
                    $image_extensions = array("png", "jpg", "jpeg", "gif");

                    // Target directory
                    $dir = 'assets/resources/bcsjourney/';
                    if (is_dir($dir)) {

                        if ($dh = opendir($dir)) {
                            $count = 1;

                            // Read files
                            while (($file = readdir($dh)) !== false) {

                                if ($file != '' && $file != '.' && $file != '..') {

                                    // Thumbnail image path
                                    $thumbnail_path = "assets/resources/bcsjourney/thumbnails/" . $file;

                                    // Image path
                                    $image_path = "assets/resources/bcsjourney/" . $file;

                                    $thumbnail_ext = pathinfo($thumbnail_path, PATHINFO_EXTENSION);
                                    $image_ext = pathinfo($image_path, PATHINFO_EXTENSION);

                                    // Check its not folder and it is image file
                                    if (
                                        !is_dir($image_path) &&
                                        in_array($thumbnail_ext, $image_extensions) &&
                                        in_array($image_ext, $image_extensions)
                                    ) {
                    ?>

                                        <!-- Image -->
                                        <a href="<?php echo $image_path; ?>">
                                            <img src="<?php echo $thumbnail_path; ?>" alt="" title="" />
                                        </a>
                                        <!-- --- -->
                                        <?php

                                        // Break
                                        if ($count % 4 == 0) {
                                        ?>
                                            <div class="clear"></div>
                    <?php
                                        }
                                        $count++;
                                    }
                                }
                            }
                            closedir($dh);
                        }
                    }
                    ?>
                </div>
            </div>

            <a href="https://player.vimeo.com/video/606238976" data-docid="e23986d9878de432bef0df362e92c43009b452dfd08ef09f57a49e16919faa2d" class="viewvideo resdl" id="bcsVideo01">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://player.vimeo.com/video/606238116" data-docid="a9a8e59c5bbfe848711983f3d69d7434e1269618c96e5c7324cb68d42347c13f" class="viewvideo resdl" id="bcsVideo02">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://player.vimeo.com/video/606238279" data-docid="a303604dee57829ba80259dde1a18c99d9061d9a660123fb5caf15265e503b65" class="viewvideo resdl" id="bcsVideo03">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://player.vimeo.com/video/608783497" data-docid="a451467bbbe9097d49a0f117cfa8c837641f64f5a0e16a96a2291f02cb6de0f8" class="viewvideo resdl" id="bcsVideo04">
                <div class="indicator d-6"></div>
            </a>


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script type="text/javascript" src="assets/js/simple-lightbox.jquery.min.js"></script>
<script type='text/javascript'>
    $(document).ready(function() {
        // Intialize gallery
        var gallery = $('.gallery a').simpleLightbox();

    });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>