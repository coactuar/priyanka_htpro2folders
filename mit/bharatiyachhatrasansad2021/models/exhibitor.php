<?php

class Exhibitor
{
    private $ds;

    private $res_id;
    private $res_title;
    private $res_url;
    private $restable = 'tbl_resources';
    private $resdltable = 'tbl_resourcedownloads';

    private $user_id;
    private $usertable = 'tbl_users';

    function __construct()
    {
        $this->ds = new DataSource();
    }
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function addExhibRes()
    {
        $this->res_id = bin2hex(random_bytes(32));
        $query = 'INSERT INTO ' . $this->restable . '(resource_id, resource_title, resource_url) values(?,?,?)';
        $paramType = 'sss';
        $paramValue = array(
            $this->res_id,
            $this->res_title,
            $this->res_url
        );

        $resId = $this->ds->insert($query, $paramType, $paramValue);

        if (!empty($resId)) {
            $response = setResponse('success', 'New resource added');
        } else {
            $response = setResponse('error', 'New resource could not be added');
        }

        return $response;
    }

    public function getResources()
    {
        $query = 'Select resource_id, resource_title, resource_url, download_count from ' . $this->restable . ' order by resource_title';
        $paramType = '';
        $paramValue = array();

        $resources = $this->ds->select($query, $paramType, $paramValue);

        return $resources;
    }
    public function getResourceList()
    {
        $query = 'Select resource_id, resource_title, resource_url, download_count from ' . $this->restable . ' where download_count > 0 order by resource_title';
        $paramType = '';
        $paramValue = array();

        $resources = $this->ds->select($query, $paramType, $paramValue);

        return $resources;
    }

    public function getResource()
    {
        $query = 'Select resource_id, resource_title,resource_url, download_count from ' . $this->restable . ' where resource_id= ?';
        $paramType = 's';
        $paramValue = array($this->res_id);

        $resource = $this->ds->select($query, $paramType, $paramValue);

        return $resource;
    }

    public function updResource()
    {
        $query = 'Update ' . $this->restable . ' set resource_title=?, resource_url=? where resource_id = ?';
        $paramType = 'sss';
        $paramValue = array(
            $this->res_title,
            $this->res_url,
            $this->res_id
        );

        $this->ds->execute($query, $paramType, $paramValue);
        $response = setResponse('success', 'Resource updated');

        return $response;
    }

    public function delResource()
    {
        $query = 'delete from ' . $this->restable . ' where resource_id = ?';
        $paramType = 's';
        $paramValue = array(
            $this->res_id
        );

        $this->ds->execute($query, $paramType, $paramValue);
        $response = setResponse('success', 'Resource deleted');

        return $response;
    }

    public function updateFileDLCount()
    {
        $res = $this->getResource();
        $dl = $res[0]['download_count'] + 1;

        $query = 'Update ' . $this->restable . ' set download_count=? where resource_id = ?';
        $paramType = 'ss';
        $paramValue = array(
            $dl,
            $this->res_id
        );

        $this->ds->execute($query, $paramType, $paramValue);

        $dltime   = date('Y/m/d H:i:s');
        $query = "Insert into " . $this->resdltable . "(resource_id, user_id, dl_time) values(?, ?, ?)";
        $paramType = 'sss';
        $paramValue = array(
            $this->res_id,
            $this->user_id,
            $dltime
        );
        $dlid = $this->ds->insert($query, $paramType, $paramValue);
        return $dlid;
    }

    public function getResDownloads()
    {
        $query = 'Select distinct(' . $this->resdltable . '.user_id ), emailid from ' . $this->resdltable . ', ' . $this->usertable . ' where resource_id=? and ' . $this->resdltable . '.user_id = ' . $this->usertable . '. userid';
        $paramType = 's';
        $paramValue = array($this->res_id);

        $dlList = $this->ds->select($query, $paramType, $paramValue);

        return $dlList;
    }


    public function getResCount()
    {
        $query = "SELECT distinct(resource_id) FROM " . $this->restable;
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getResDLCount()
    {
        $query = "select sum(download_count) as total from " . $this->restable;
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }
}
