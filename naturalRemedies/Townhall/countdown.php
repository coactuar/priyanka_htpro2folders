<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
html, body{
    height:100%;
}
body {
    font-family: 'Roboto', sans-serif;
    font-size: 100px;
    background-color: #ffffff;
    
    margin:0;
    padding:0;
    
}
h6{
    font-size:0.3em;
    margin-bottom:0;
}
div.container{
    width:100%;
    height:100%;
}
div#countdown {
    font-size: 4.0em;
    color: #000000;
    width: fit-content;
    height: auto;
    position:absolute;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%);
}
@media only screen and (max-width: 2200px) {
    div#countdown {
        font-size: 3.5em;
    }
}
@media only screen and (max-width: 1950px) {
    div#countdown {
        font-size: 3.0em;
    }
}
@media only screen and (max-width: 1650px) {
    div#countdown {
        font-size: 2.5em;
    }
}
@media only screen and (max-width: 1350px) {
    div#countdown {
        font-size: 2.0em;
    }
}
@media only screen and (max-width: 1150px) {
    div#countdown {
        font-size: 1.5em;
    }
}
@media only screen and (max-width: 850px) {
    div#countdown {
        font-size: 1.0em;
    }
}
@media only screen and (max-width: 600px) {
    div#countdown {
        font-size: 1.0em;
    }
}
</style>
</head>
<body>
<div class="container">
    <div id="countdown" align="center"></div>
</div>
<script>
CountDownTimer('10/12/2020 08:30 PM', 'countdown');

function CountDownTimer(dt, id)
{
    var end = new Date(dt);

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance <=1) {

            clearInterval(timer);
            //document.getElementById(id).innerHTML = 'EXPIRED!';
            location.href='video.php';

            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        document.getElementById(id).innerHTML = '<h6>EVENT WILL START IN</h6>';
        document.getElementById(id).innerHTML += padLeadingZeros(days,2) + ':';
        document.getElementById(id).innerHTML += padLeadingZeros(hours,2) + ':';
        document.getElementById(id).innerHTML += padLeadingZeros(minutes,2) + ':';
        document.getElementById(id).innerHTML += padLeadingZeros(seconds, 2) + '';
    }

    timer = setInterval(showRemaining, 1000);
}

function padLeadingZeros(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
</script>
</body>
</html>
