<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Amity gallery final render Edit Reflections.jpg">
            <a href="assets/img/Clickable poster session/1st row 9th right last/poster_1.jpg" id="exhVideo " class="view viewvideo" data-vidid="11345" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/1st row left top/poster2.jpg" id="exhVideo1" class="view exhVideo1" data-vidid="11345" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/Nusrat Nabi PCTAIRE1 poster SBCI Nov 2021.png" id="exhVideo2" class="view exhVideo2" data-vidid="1" >
                <div class="indicator d-6"></div>
            </a>
             <a href="assets/img/Clickable poster session/2nd row 8th centre/poster4.jpg" id="exhVideo3" class="view exhVideo3" data-vidid="2">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo4" class="view exhVideo4" data-vidid="3">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/Nusrat Nabi PCTAIRE1 poster SBCI Nov 2021.pdf" id="exhVideo5" class="showpdf exhVideo5" data-vidid="4">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/Nano Mission Project Slide_page-0001.jpg" id="exhVideo6" class="view exhVideo6" data-vidid="5">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo7" class="view exhVideo7" data-vidid="6">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo8" class="view exhVideo8" data-vidid="7">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo9" class="view exhVideo9" data-vidid="8">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/poster5.jpg" id="exhVideo10" class="view exhVideo10" data-vidid="9" >
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/img/Clickable poster session/2nd row last right/Nusrat Nabi PCTAIRE1 poster SBCI Nov 2021.png" id="exhVideo11" class="view exhVideo11" data-vidid="10">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="assets/resources/2.Sample Posters - KB_RGM_new grant  -  Compatibility Mode.pdf" id="exhVideo" class="showpdf"></a> -->

            <a href="bondk.php" id="bondk" class="bondk" data-vidid="21">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="colsmartA.php" id="colsmartA">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="lizolid.php" id="lizolid">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="stiloz.php" id="stiloz">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamp.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <div id="next-button">
                <a href="posterseession.php"><i class="fas fa-arrow-alt-circle-right"></i> Next</a>
            </div> -->
        </div>
     
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<script>
    $(function() {
        $('.exhVideo1').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo2').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo3').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo4').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });

    $(function() {
        $('.exhVideo5').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo6').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo7').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo8').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });

    $(function() {
        $('.exhVideo9').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo10').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.exhVideo11').on('click', function() {
            var vid_id = $(this).data('vidid');
            
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
    $(function() {
        $('.bondk').on('click', function() {
            var vid_id = $(this).data('vidid');
            // alert('vid_id');
            var userid="<?php echo $_SESSION['userid']; ?>"
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },

                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>