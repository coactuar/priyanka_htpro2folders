<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {

$action = $_POST['action'];

switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }

            $start_from = ($page-1) * $limit;

            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Logged In Users: <?php 

                    $sql = "SELECT COUNT(id) as count FROM tbl_users where login_date != '0000-00-00 00:00:00' and batch='".$_SESSION['admin_batch']."'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
                    echo $total_records; 

                    ?>
                </div>
                <!--<div class="col-6">
                    Total Registered Users: <?php 

                    $sql = "SELECT COUNT(id) as count FROM tbl_users where batch='".$_SESSION['admin_batch']."'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_reg = $row['count'];  
                    echo $total_reg; 

                    ?>
                </div>-->
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Batch No</th>
                          <th>Phone No.</th>
                          <th>Registration Time</th>
                          <th>Last Login Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        //$query="select * from tbl_users where login_date != '0000-00-00 00:00:00' and batch='".$_SESSION['admin_batch']."' order by login_date desc LIMIT $start_from, $limit";
                        $query="select * from tbl_users where batch='".$_SESSION['admin_batch']."' order by login_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?></td>
                            <td><?php echo $data['batch']; ?></td>
                            <td><?php echo '+'.$data['cntry_code'] . ' - ' . $data['mobile_num']; ?></td>
                            <td><?php 
                                if($data['joining_date'] != ''){
                                    $date=date_create($data['joining_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '<small>Never Logged in.<small>';
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;



}
}
?>