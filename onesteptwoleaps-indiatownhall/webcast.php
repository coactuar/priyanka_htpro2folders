
<?php

	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["user_email"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where email='$user_email'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            
            header("location: https://coact.live/mammographyusermeet/login.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Seimens</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/all.min.css">

  <link rel="stylesheet" href="assets/css/styles.css">
<style>
textarea {
  resize: none;
}

</style>
</head>
<body>
    



    <div class="page-content">
        <div id="content">
       
            <div id="bg">
                <img src="assets/img/Seimens.png">
                
                <div id="webcast-area" style="top: 25.5%;
    left: 25.5%;
    width: 47.8%;
    height: 48%;">
                    <!-- <a id="goFS" href="#" class="fs">Fullscreen</a> -->
                     <!--<iframe src="https://vimeo.com/event/674687/embed" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>-->
					 <iframe src="video.php" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
                </div>
            </div>
            <div id="ask-ques" style="width:110px">
                <a href="#" id="askques"><i class="fas fa-question-circle"></i>ASK QUESTIONS</a>
				
				
                <!-- <button class="toggle">Show &amp; Hide</button>
    <div id="target"></div> -->
    
            </div>
         <div id="take-poll">
               <a class="btn btn-sm btn-secondary float-left " style="" href="?action=logout">Logout</a>
            </div> 
            <div class="panel ques" >
                <div class="panel-heading">
                   ASK QUESTIONS
                    <a href="#" class="close" id="close_ques"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                <div id="question" class=" ">
                
              <div id="question-form" class="">
              <form method="POST" action="#" class="form panel-body" role="form">
                  <div class="row">
                      <div class="col-md-12">
                      <div id="ques-message"></div>
                      <div class="form-group">
                          <textarea class="form-control text_area" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                      </div>
                      
                      </div>
                      <div class="col-12 ">
                      <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                      <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_email']; ?>">
                      <button class="btn btn-primary btn-sm" type="submit">Submit your Question</button>
                      </div>
                  </div>
            </form>
           
          </div>
          
         
      </div>
   
    </div>
                </div>
    
            </div>
            
    </div>
    
		
          
        
		 
		
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>

$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);
/***
function getQues()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'getques'},
         type: 'post',
         success: function(output) {
             $('#asked-ques').html(output);
         }
});
}
getQues();
setInterval(function(){ getQues(); }, 10000);
**/



</script>
<script>  
// $('.toggle').click(function() {
//     $('#target').toggle('slow');
// });
$(document).on('click', '#askques', function() {
      $('.poll').removeClass('show');
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '#close_ques', function() {
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '#takepoll', function() {
      $('.ques').removeClass('show');
      $('.poll').toggleClass('show');
    });

    $(document).on('click', '#close_poll', function() {
      $('.poll').toggleClass('show');
    });
</script> 

</body>
</html>