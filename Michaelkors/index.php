<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div class="container-fluid">
    <div class="row">
    <img src="img/lptop.jpg" class="img-fluid desktopon" alt=""/> 
    <img src="img/lptop-mobile.jpg" class="img-fluid mobileon" alt=""/> 
    </div>
</div>    
<div class="container">  
    <h6 class="text-center"><strong>Welcome to the Digital Trunk Show with Lama Jamal</strong></h6>  
    <div class="login-area">
        <div class="row">
          <div class="col-12 col-md-6 d-block d-sm-none">
            <img src="img/ng-1.jpg" class="img-fluid" alt=""/>
          </div>
          <div class="col-12 col-md-6">
            <div class="login">
              <h6 class="text-center"><strong>Sign in below to discover the <br>Michael Kors Fall 2020 Arrivals.</strong></h6>
                  <div id="login-message"></div>
                  <form id="login-form" method="post" role="form">
                        <div class="form-group">
                              <input type="text" class="form-control" placeholder="Full Name"  name="name" id="name" required>
                        </div>
                        <div class="form-group">
                              <input type="email" class="form-control" placeholder="Email" name="empid" id="empid" required>
                        </div>
                        <div class="form-group">
                              <input type="text" class="form-control" placeholder="Country of Residence" name="country" id="country" required>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="exampleCheck1"> By clicking OPT IN, you confirm that you have read and agree to our Privacy Policy
                          <br/>
                          <a href="terms_conditions.pdf" target="_blank">View Privacy Policy</a></label>
                        </div>
                        <div class="form-group text-center mt-4">
                              <input type="submit" class="submit"  alt="Submit" value="Join Event">
                        </div>
                  </form>
               </div>   
          </div>
          <div class="col-12 col-md-6 d-none d-sm-block">
            <img src="img/ng-1.jpg" class="img-fluid" alt=""/> 
          </div>
      
      </div>
    
    </div>
    

</div>

<!--<div class="container-fluid">
    <div class="row top-heading">
        <div class="col-12 col-md-12 text-center" >
        <img src="img/lptop.jpg" class="img-fluid" alt=""/> 
        </div>
	</div>

    <div class="login-area">
    <div class="content">
    <div class="row mt-2 mb-2">
    
        <div class="resps col-md-12 col-lg-4 offset-lg-2">
        
        <img src="img/ng (1).jpg" class="img-fluid imgmob" alt="">
        <p class="col-12 col-md-12 form-group respara">Welcome to Digital Trunk Show with Lama Jamal Sign in below to discover the MICHAEL Michael Kors Fall 2020 collection</p>
            <div class="login">
                <div id="login-message"></div>
                <form id="login-form" method="post" role="form">
                      <div class="row">
						<div class="col-2 col-md-2 form-group">
						<label><p class="text12">Name</p></label>
						</div>
                        <div class="col-10 col-md-10 form-group">
                            <input type="text" class="form-control" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                        </div>
						<div class="col-2 col-md-2 form-group">
							<label><p class="text12">Email</p></label>
						</div>
						 <div class="col-10 col-md-10 form-group">
						 
                            <input type="email" class="form-control" aria-label="Email" aria-describedby="basic-addon1" name="empid" id="empid" required>
                        </div>
						<div class="col-2 col-md-2 form-group">
							 <label><p class="text12">Country of residence</p></label>
						</div>
						 <div class="col-10 col-md-10 form-group">
						
                            <input type="text" class="form-control" aria-label="Country" aria-describedby="basic-addon1" name="country" id="country" required>
                        </div>
						 <div class="form-check col-12 col-md-7 offset-lg-3 offset-md-3">
						<input type="checkbox" class="form-check-input" id="exampleCheck1" required>
						<label class="form-check-label" for="exampleCheck1">Subscribe to receive thank you gift
						<br/>
							<a href="#">view terms</a></label>
						</div>
						
                        <div class="col-12 col-md-11 text-center p-2">
                            <input type="submit"  class="submit"  alt="Submit" value="Join Event">
                        </div>
                        
                      </div>
					 
                </form>
				
            </div>
			
        </div>
		<div class="col-12 col-md-12 col-lg-4 offset-lg-2 ">
            
                <img src="img/ng (1).jpg" class="img-fluid iresponsive" alt="">
               
           
        </div>
		 
    </div>
    </div>
    </div>
    
    
    
</div>-->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn();//.delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-16"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-16');
</script>

</body>
</html>