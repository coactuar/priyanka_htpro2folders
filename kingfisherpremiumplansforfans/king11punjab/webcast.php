<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_name"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_id=$_SESSION["user_id"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where id='$user_id'  and eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_id"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="bg1">

<nav class="navbar m-0 p-0">
        <img src="img/planswithfans.png" width="150px" class="text-center mobile" alt="">
        <img src="img/kingfisherlogo.png "  class="logout mobile" width="150px" alt="">
        <img src="img/kp-logo.png" class="float-right mobile_view mobile" width="150px" alt=""> 
   
</nav>
<div class="container-fluid">
  <div class="">
      <div class="row mt-2 mb-2 p-2">
            <div class="col-12 col-md-8 text-center">
                <div class="embed-responsive">
                 <div style="padding:56.25% 0 0 0;position:relative;">
                   <iframe src="https://vimeo.com/event/1283930/embed" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                 </div>
                
                </div>
          </div>
            <div class="col-12 col-md-4">
                <div class="login">
                    <div id="polls" style="display:none;">
                      <div class="row mt-2">
                          <div class="col-12">
                              <iframe id="poll-question" src="#" width="100%" height="220" frameborder="0" scrolling="no"></iframe>
                          </div>
                      </div>    
                    </div>
                    <div class="login-form">
                        <form id="question-form" method="post" role="form">
                          <div class="row">
                            <div class="col-10">
                                <h6 class="text-dark">Comments</h6>
                                <div class="form-group">
                                   <textarea class="form-control" name="userQuestion" id="userQuestion" required rows="20"></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="row mt-3">
                            <div class="col-10">
                              <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                              <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                                <input type="submit" class="btn btn-success" value="submit" alt="Submit">
                                <div id="message"></div>
                            </div>
                          </div>  
                    </form>
                    </div>
                </div>
                <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 3000);
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-15');
</script>

</body>
</html>