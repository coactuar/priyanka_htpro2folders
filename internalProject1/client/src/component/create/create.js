import React, { useState, useEffect } from 'react';
import { Form, Button } from 'semantic-ui-react';
import axios from 'axios';



export default function Create() {
    const [firstName, setfirstName] = useState('');
    const [jobName, setJobName] = useState('');
    console.log(firstName);
    console.log(jobName);
    const submitReview = () => {
        axios.post('https://61acfdcbd228a9001703acb3.mockapi.io/internalpro', {
            firstName,
            jobName
        })
    }
    return ( <
        div >
        <
        Form >
        <
        form className = "ui form" >
        <
        div className = "field" >
        <
        label > First Name < /label> <
        input type = "text"
        name = "empname"
        onChange = {
            (e) => {
                setfirstName(e.target.value)
            }
        }
        /> <
        /div> <
        div className = "field" >
        <
        label > Job title < /label> <
        input type = "text"
        name = "job"
        onChange = {
            (e) => {
                setJobName(e.target.value)
            }
        }
        /> <
        /div> <
        button type = "submit"
        onClick = { submitReview } > Submit < /button> <
        /form> <
        /Form> <
        /div>
    )
}