<form id="form-ava" action="../config.inc/upload.php" method="post" enctype="multipart/form-data">
     <input type="hidden" id="id_user" name="id_user" value="<?php echo $row->id_user;?>">
            <input id="ava-img" type="file" name="image" value="<?php echo $row->image;?>"/>
                <input id="button" type="submit" value="Simpan" name="update"></br>
     <a href="#" class="btn btn-large btn-success" id="cancel-act"></i> &nbsp; Batal</a>
 </form>
   <div id="err"></div>


<script type="text/javascript">
$("#form-ava").on('submit',(function(e) {
e.preventDefault();
$.ajax({
    url: "../config.inc/upload.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: false,
    processData:false,

    success: function(data)
    {
        if(data=='invalid')
        {
            // invalid file format.
            $("#err").html("Invalid File !").fadeIn();
        }
        else
        {
            // view uploaded file.
            $("#preview-ava").html(data).fadeIn();
            $("#form-ava")[0].reset();
  $("#hidden-list").hide();
        }
    },
    error: function(e)
    {
        $("#err").html(e).fadeIn();
    }
});
}));
</script>

<!doctype html>
<html lang="en">
  <head>
    <title>
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>