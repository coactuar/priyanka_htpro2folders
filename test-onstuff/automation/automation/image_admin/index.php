<?php
  include("../config.php");
  if(empty($_SESSION['csrf'])){
    $_SESSION['csrf'] = bin2hex(random_bytes(32));
  }
$token = $_SESSION['csrf'];



$images_sql = "SELECT * FROM images ";
$result = mysqli_query($link,$images_sql);
if (mysqli_num_rows($result) > 0) {
while($row = mysqli_fetch_assoc($result)){
  $filename[] = $row['img_url'];
}
  
}
  
 // $filename +='image_admin/'.$filename;




?>
<html>

<head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <base target="_self">
    <meta name="description" content="A Bootstrap 4 admin dashboard theme that will get you started. The sidebar toggles off-canvas on smaller screens. This example also include large stat blocks, modal and cards. The top navbar is controlled by a separate hamburger toggle button."
    />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google" value="notranslate">
    <link rel="shortcut icon" href="/images/cp_ico.png">


    <!--stylesheets / link tags loaded here-->


    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />


    <style type="text/css">
        body,
        html {
            height: 100%;
        }
        /* workaround modal-open padding issue */
        
        body.modal-open {
            padding-right: 0 !important;
        }
        

        
        .card {
            overflow: hidden;
        }
        
        .card-block .rotate {
            z-index: 8;
            float: right;
            height: 100%;
        }
        
        .card-block .rotate i {
            color: rgba(20, 20, 20, 0.15);
            position: absolute;
            left: 0;
            left: auto;
            right: -10px;
            bottom: 0;
            display: block;
            -webkit-transform: rotate(-44deg);
            -moz-transform: rotate(-44deg);
            -o-transform: rotate(-44deg);
            -ms-transform: rotate(-44deg);
            transform: rotate(-44deg);
        }
    </style>

</head>

<body>
<?php include('menu_bar.php');?>
         
                <h1 class="display-5 hidden-xs-down">
              LOGO
                </h1>
                <p class="lead hidden-xs-down"></p>

                <div class="alert alert-warning fade collapse" role="alert" id="myAlert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                    <strong>AJANTHA</strong>
                </div>

                <div class="row mb-3">
                    <div class="col-xl-4 col-lg-6">
                        <div class="card card-inverse card-success">
                            <div class="card-block bg-success">
                                <div class="rotate">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div id="err"></div>
                                <form method="POST" id="uploading" action="" enctype='multipart/form-data'>
                                <input type="hidden" name="page" value="landing_page">  		                            
                                <input type="hidden" name="img_name" value="logo">  		                            
                                <input type="hidden" name="action" value="getimageupdate">  
                                	                            
                                <input type="hidden" name="token" value="<?= $token; ?>">  		                            
                                 <p>
                                 <input type='file'  id="upload_imgs" name='file'  multiple/>
                                </p>
                                <input type='submit' value='Save name' name='but_upload'>
                                       </form>
                        </div>
                        </div>
                     </div>
                     <div class="col-xl-6 col-lg-6">
                          <div class="card card-inverse card-danger">
                            <div class="card-block bg-danger">
                               
                                <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden " id="img_preview" aria-live="polite" >
                                <div id="preview-ava"></div>
                               <div id="img_live">
                               
                               <img src="<?= $filename[0]; ?>"  class="img-fluid" alt="">

                               </div>
                                
                                </div>
                             </div>
                             </div>
                              </div>
                              <div class="col-xl-4 col-lg-6 mt-2">
                              <div class="card card-inverse card-info">
                              <div class="card-block bg-info">
                              <form method="post" id="uploading1" action="" enctype='multipart/form-data'>
                              <input type="hidden" name="page" value="landing_page">  		                            
                                <input type="hidden" name="img_name" value="banner">
                                <input type="hidden" name="action" value="playerupdate">  
                                <input type="hidden" name="token" value="<?= $token; ?>">  	
                               <p> 
                                 <input type='file'  id="upload_imgs" name='file' multiple/>
                                 <!-- <input type="file" id="dvd_image" /> -->
                               </p>
    
                                     <input type='submit' value='Save name' name='but_upload'>

                             </form>
                            </div>
                        </div>
                     </div>
                     <div class="col-xl-6 col-lg-6 mt-2">
                        <div class="card card-inverse card-warning">
                            <div class="card-block bg-warning">
                            <div id="preview-ava"></div>
      <img src="<?= $filename[1]; ?>"  class="img-fluid" alt="">

                           
                            </div>
                        </div>
                    </div>
                </div>
               <hr>
       <h1 class="display-5 hidden-xs-down">
              Main Images
                </h1>
               <div class="row mb-3">
                    <div class="col-xl-4 col-lg-6">
                        <div class="card card-inverse card-success">
                            <div class="card-block bg-success">
                                <div class="rotate">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <form method="post" id="uploading2" action="" enctype='multipart/form-data'>
                              <input type="hidden" name="page" value="webcastpage">  		                            
                                <input type="hidden" name="img_name" value="mainimage">
                                <input type="hidden" name="action" value="mainimageupdate">  
                                <input type="hidden" name="token" value="<?= $token; ?>"> 
                               <p> 
                                 <input type='file'  id="upload_imgs" name='file' multiple/>
                                 <!-- <input type="file" id="dvd_image" /> -->
                                
                               </p>
    
                                     <input type='submit' value='Submit'>

                             </form>
                        </div>
                        </div>
                     </div>
                     <div class="col-xl-6 col-lg-6">
                          <div class="card card-inverse card-danger">
                            <div class="card-block bg-danger">
                               
                             
                           
                              <img src="<?= $filename[2]; ?>"  class="img-fluid" alt="">
      
                             </div>
                             </div>
                              </div>
                                  <div class="col-xl-4 col-lg-6 mt-2">
                              <div class="card card-inverse card-info">
                              <div class="card-block bg-info">
                              <form method="post" id="uploading3" action="" enctype='multipart/form-data'>
                              <input type="hidden" name="page" value="webcastpage">  		                            
                                <input type="hidden" name="img_name" value="webcastimage">
                                <input type="hidden" name="action" value="webcastimageupdate">  
                                <input type="hidden" name="token" value="<?= $token; ?>"> 
                               <p> 
                                 <input type='file'  id="upload_imgs" name='file' multiple/>
                                 <!-- <input type="file" id="dvd_image" /> -->
                               </p>
    
                                     <input type='submit' value='Save name' name='but_upload'>

                             </form>
                            </div>
                        </div>
                     </div>
                     <div class="col-xl-6 col-lg-6 mt-2">
                        <div class="card card-inverse card-warning">
                            <div class="card-block bg-warning">
                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden " id="img_preview" aria-live="polite">
                          
                            </div>
                            <div class="preview2 hide2">
        <img src="<?= $filename[3]; ?>" class="image_to_upload2 img-fluid"/>
      </div>
                            </div>
                        </div>
                    </div>
                </div>
            


    </div>
    <footer class="container-fluid">
        <p class="text-right small"></p>
    </footer>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                </div>
                <div class="modal-body">
                  about
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary-outline" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   


  <script type="text/javascript">


$("#uploading").on('submit',(function(e){
  
e.preventDefault();
$.ajax({
url: "ajaxnew.php",
type: "POST",
data:  new FormData(this),
contentType: false,
cache: false,
processData:false,
success: function(data)
    {
        if(data=='invalid')
        {
            // invalid file format.
            $("#err").html("Invalid File !").fadeIn();
        }
        else
        {
            // view uploaded file.
           $("#preview-ava").html(data).fadeIn();
            $("#uploading")[0].reset();
  $("#hidden-list").hide();
        }
    },
    error: function(e)
    {
        $("#err").html(e).fadeIn();
    }
});
}));
$("#uploading1").on('submit',(function(e){
  
  e.preventDefault();
  $.ajax({
  url: "ajaxnew.php",
  type: "POST",
  data:  new FormData(this),
  contentType: false,
  cache: false,
  processData:false,
  success: function(data)
      {
          if(data=='invalid')
          {
              // invalid file format.
              $("#err").html("Invalid File !").fadeIn();
          }
          else
          {
              // view uploaded file.
             $("#preview-ava").html(data).fadeIn();
              $("#uploading1")[0].reset();
    $("#hidden-list").hide();
          }
      },
      error: function(e)
      {
          $("#err").html(e).fadeIn();
      }
  });
  }));
  $("#uploading2").on('submit',(function(e){
  
  e.preventDefault();
  $.ajax({
  url: "ajaxnew.php",
  type: "POST",
  data:  new FormData(this),
  contentType: false,
  cache: false,
  processData:false,
  success: function(data)
      {
          if(data=='invalid')
          {
              // invalid file format.
              $("#err").html("Invalid File !").fadeIn();
          }
          else
          {
              // view uploaded file.
             $("#preview-ava").html(data).fadeIn();
              $("#uploading2")[0].reset();
    $("#hidden-list").hide();
          }
      },
      error: function(e)
      {
          $("#err").html(e).fadeIn();
      }
  });
  }));
  $("#uploading3").on('submit',(function(e){
  
  e.preventDefault();
  $.ajax({
  url: "ajaxnew.php",
  type: "POST",
  data:  new FormData(this),
  contentType: false,
  cache: false,
  processData:false,
  success: function(data)
      {
          if(data=='invalid')
          {
              // invalid file format.
              $("#err").html("Invalid File !").fadeIn();
          }
          else
          {
              // view uploaded file.
             $("#preview-ava").html(data).fadeIn();
              $("#uploading3")[0].reset();
    $("#hidden-list").hide();
          }
      },
      error: function(e)
      {
          $("#err").html(e).fadeIn();
      }
  });
  }));
</script>
      
 

</body>

</html>