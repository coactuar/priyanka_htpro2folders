const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());
const db = mysql.createConnection({
    user: 'root',
    host: 'localhost',
    password: '',
    database: 'internal_project'
});


app.get("/", (req, res) => {
    res.send("hello world");
});

app.post('/create', (req, res) => {
    console.log(req.body);
    const empname = req.body.empname
    const job = req.body.job
    const department = req.body.department
    const eventname = req.body.eventname
    const dateevent = req.body.dateevent
    const timeevent = req.body.timeevent
    const approve = req.body.approve
    const date1 = req.body.date1
    const date2 = req.body.date2
    const date3 = req.body.date3
    const stay = req.body.stay
    const landingtime = req.body.landingtime
    const sign = req.body.sign

    db.query('INSERT INTO internal(empname,job,department,eventname,dateevent,timeevent,approve,date1,date3,date2,stay,landingtime,sign) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)', [empname, job, department, eventname, dateevent, timeevent, approve, date1, date3, date2, landingtime, stay, sign],
        (err, result) => {
            if (err) {
                console.log(err + 'xyd');
            } else {
                res.send("Values Inserted");
            }

        });
});

app.get('/employees', (req, res) => {
    db.query("SELECT * FROM internal", (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send(result); //send the request from backend to frontend
        }
    });
})

app.listen(3001, () => {
    console.log("This server side");
});